%define debug_package %{nil}

Name:            gala-anteater
Version:         1.1.0
Release:         1
Summary:         A time-series anomaly detection platform for operating system.
License:         MulanPSL2
URL:             https://gitee.com/openeuler/gala-anteater
Source:          %{name}-%{version}.tar.gz
BuildRoot:       %{_builddir}/%{name}-%{version}
BuildRequires:   procps-ng python3-setuptools
Requires:        python3-gala-anteater = %{version}-%{release}

%description
Abnormal detection module for A-Ops project

%package -n python3-gala-anteater
Summary:         Python3 package of gala-anteater
Requires:        python3-APScheduler python3-kafka-python python3-joblib python3-numpy
Requires:        python3-pandas python3-requests python3-scikit-learn python3-pytorch
Requires:        python3-pyyaml

%description -n python3-gala-anteater
Python3 package of gala-anteater

%prep
%autosetup -n %{name}-%{version} -p1

%build
%py3_build

%install
%py3_install

%pre
if [ -f "%{_unitdir}/gala-anteater.service" ] ; then
        systemctl enable gala-anteater.service || :
fi

%post
%systemd_post gala-anteater.service

%preun
%systemd_preun gala-anteater.service

%postun
if [ -f "%{_unitdir}/gala-anteater.service" ] ; then
	%systemd_postun_with_restart gala-anteater.service
fi

%files
%doc README.md
%license LICENSE
%{_bindir}/gala-anteater
%config(noreplace) %{_sysconfdir}/%{name}/config/metricinfo.json
%config(noreplace) %{_sysconfdir}/%{name}/config/gala-anteater.yaml
%config(noreplace) %{_sysconfdir}/%{name}/config/log.settings.ini
%config(noreplace) %{_sysconfdir}/%{name}/module/app_sli_rtt.job.json
%config(noreplace) %{_sysconfdir}/%{name}/module/disk_throughput.job.json
%config(noreplace) %{_sysconfdir}/%{name}/module/jvm_oom.job.json
%config(noreplace) %{_sysconfdir}/%{name}/module/proc_io_latency.job.json
%config(noreplace) %{_sysconfdir}/%{name}/module/sys_io_latency.job.json
%config(noreplace) %{_sysconfdir}/%{name}/module/sys_nic_loss.job.json
%config(noreplace) %{_sysconfdir}/%{name}/module/sys_tcp_establish.job.json
%config(noreplace) %{_sysconfdir}/%{name}/module/sys_tcp_transmission_latency.job.json
%config(noreplace) %{_sysconfdir}/%{name}/module/usad_model.job.json
/usr/lib/systemd/system/gala-anteater.service

%files -n python3-gala-anteater
%{python3_sitelib}/anteater/*
%{python3_sitelib}/gala_anteater-*.egg-info


%changelog
* Thu Aug 31 2023 Li Zhenxing <lizhenxing11@huawei.com> - 1.1.0-1
- Upgrade anteater version to 1.1.0

* Fri Jan 20 2023 Zhen Chen <chenzhen126@huawei.com> - 1.0.1-4
- eliminate 'Fail to try-restart' warning when downgrading to 1.0.1-1

* Thu Jan 19 2023 Zhen Chen <chenzhen126@huawei.com> - 1.0.1-3
- fix missing Requires:python3-pyyaml

* Tue Jan 17 2023 Zhen Chen <chenzhen126@huawei.com> - 1.0.1-2
- fix str2enum bug & data query refactor
- add systemd service for anteater
- remove 'sys-level' config param
- add chinese descriptions
- Update TCP Establish Model & Add Nic Loss Detector
- Add disk throughput detector

* Wed Nov 30 2022 Li Zhenxing <lizhenxing11@huawei.com> - 1.0.1-1
- Add sys level anomaly detection and cause inference

* Tue Nov 22 2022 Li Zhenxing <lizhenxing11@huawei.com> - 1.0.0-2
- Updates anomaly detection model and imporves cause inference result

* Sat Nov 12 2022 Zhen Chen <chenzhen126@huawei.com> - 1.0.0-1
- Package init
